cleanup ()
{
    docker kill ds1
    docker rm ds1
    kill -s SIGTERM $!
    exit 0
}

trap cleanup SIGINT SIGTERM


docker build -t ds .
docker run -d --name ds1 -p 8080:8080 ds

while [ 1 ]
do
    sleep 1
done