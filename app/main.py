from flask import Flask, send_file
import flask
app = Flask(__name__)
error = "<title>405 Method Not Allowed</title><h1>Method Not Allowed</h1><p>The method is not allowed for the requested URL.</p>"



@app.route("/")
def main():
    return "Flask is running"



@app.route("/hello", methods=['GET', 'POST'])
def hello():
    if flask.request.method == 'POST':
        return error, 405
    if flask.request.method == 'GET':
        return "Hello world!"



@app.route("/test", methods=['GET', 'POST'])
def test():
    if flask.request.method == 'POST':
        msg = ""
        if('msg' in flask.request.args):
            msg = flask.request.args.get('msg')
        return "POST message received: "+msg
    if flask.request.method == 'GET':
        return "GET request received"

        


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)

